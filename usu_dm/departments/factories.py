# -*- coding: UTF-8 -*-
import factory

from usu_dm.departments.models import Department


class DepartmentFactory(factory.DjangoModelFactory):
    class Meta:
        model = Department
        django_get_or_create = ('name', )

    name = factory.Sequence(lambda n: f'Department {n}')
    description = factory.Faker('paragraphs', nb=4)
    superior = factory.SubFactory('usu_dm.departments.factories.DepartmentFactory')
    leader = factory.SubFactory('usu_dm.staff.factories.PersonFactory')


__all__ = (
    'DepartmentFactory',
)
