# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.translation import gettext_lazy as _


class Department(models.Model):
    name = models.CharField(_("Name"), max_length=128)
    description = models.TextField(_("Description"), blank=True)
    superior = models.ForeignKey(
        'self', verbose_name=_("Superior"),
        null=True, on_delete=models.PROTECT, related_name='subordinated_departments')
    leader = models.ForeignKey(
        'dm_staff.Person', verbose_name=_("Leader"), on_delete=models.PROTECT, related_name='leaded_departments')

    def __str__(self):
        return self.name


__all__ = (
    'Department',
)
