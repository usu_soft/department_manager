# -*- coding: UTF-8 -*-
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class DepartmentsConfig(AppConfig):
    name = 'usu_dm.departments'
    label = 'dm_departments'
    verbose_name = _("Departments")
