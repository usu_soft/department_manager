# -*- coding: UTF-8 -*-
from django.contrib import admin

from usu_dm.departments.models import Department


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
