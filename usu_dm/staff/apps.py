# -*- coding: UTF-8 -*-
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class StaffConfig(AppConfig):
    name = 'usu_dm.staff'
    label = 'dm_staff'
    verbose_name = _("Staff")
