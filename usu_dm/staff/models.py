# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.translation import gettext_lazy as _


class Person(models.Model):
    ACTIVE_STATUS = 'Active'
    NON_ACTIVE_STATUS = 'Non-active'

    persno = models.CharField(_("Person number"), max_length=20)
    firstname = models.CharField(_("First name"), max_length=64)
    lastname = models.CharField(_("Last name"), max_length=64)
    status = models.CharField(_("Status"), max_length=32)
    salary = models.DecimalField(_("Salary"), max_digits=15, decimal_places=2)
    departments = models.ManyToManyField(
        to='dm_departments.Department',
        through='dm_staff.PersonDepartment', through_fields=('person', 'department'),
        related_name='persons')
    address = models.ForeignKey('dm_geo.Address', on_delete=models.PROTECT)

    class Meta:
        ordering = ('persno', )
        verbose_name = _("Person")
        verbose_name_plural = _("Persons")

    @property
    def full_name(self) -> str:
        return f'{self.firstname} {self.lastname}'

    def __str__(self):
        return self.full_name


class PersonDepartment(models.Model):
    person = models.ForeignKey('dm_staff.Person', on_delete=models.CASCADE, related_name='person_departments')
    department = models.ForeignKey(
        'dm_departments.Department', on_delete=models.CASCADE, related_name='person_departments')
    valid_from = models.DateTimeField(_("Valid from"))
    valid_to = models.DateTimeField(_("Valid to"))

    def __str__(self):
        return f'{self.person.full_name} <{self.department.name}>'


__all__ = (
    'Person',
    'PersonDepartment'
)
