# -*- coding: UTF-8 -*-
from django.contrib import admin

from usu_dm.staff.models import Person, PersonDepartment


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('persno', 'firstname', 'lastname', 'status')


@admin.register(PersonDepartment)
class PersonDepartmentAdmin(admin.ModelAdmin):
    list_display = ('person', 'department', 'valid_from', 'valid_to')
