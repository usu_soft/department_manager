# -*- coding: UTF-8 -*-
from datetime import datetime
from string import digits

import factory
from django.utils import timezone
from factory import fuzzy

from usu_dm.departments.factories import DepartmentFactory
from usu_dm.staff.models import Person, PersonDepartment


class PersonFactory(factory.DjangoModelFactory):
    class Meta:
        model = Person
        django_get_or_create = ('persno', )

    persno = factory.Sequence(lambda n: 'PN_{}'.format(fuzzy.FuzzyText(chars=digits, length=17).fuzz()))
    firstname = factory.Faker('first_name')
    lastname = factory.Faker('last_name')
    status = factory.Iterator((Person.ACTIVE_STATUS, Person.NON_ACTIVE_STATUS))
    salary = fuzzy.FuzzyDecimal(1000, 10000)
    address = factory.SubFactory('usu_dm.geo.factories.AddressFactory')


class PersonDepartmentFactory(factory.DjangoModelFactory):
    class Meta:
        model = PersonDepartment

    person = factory.SubFactory(PersonFactory)
    department = factory.SubFactory(DepartmentFactory)
    valid_from = datetime(2019, 1, 1, tzinfo=timezone.get_current_timezone())
    valid_to = fuzzy.FuzzyDateTime(datetime(2019, 1, 1, tzinfo=timezone.get_current_timezone()))


__all__ = (
    'PersonFactory',
    'PersonDepartmentFactory'
)
