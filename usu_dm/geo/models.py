# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.translation import gettext_lazy as _

from usu_dm.core.models.fields import UUIDField


class Country(models.Model):
    id = UUIDField()
    name = models.CharField(_('Name'), max_length=256)
    alpha3_code = models.CharField(_('Alpha 3 Code'), max_length=3)

    class Meta:
        ordering = ('name',)
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')

    def __str__(self):
        return self.name


class Address(models.Model):
    street = models.CharField(_("Street"), max_length=128)
    city = models.CharField(_("City"), max_length=128)
    state = models.CharField(_("State"), max_length=128, blank=True)
    county = models.CharField(_("County"), max_length=128, blank=True)
    country = models.ForeignKey(Country, verbose_name=_("Country"), on_delete=models.PROTECT)

    class Meta:
        ordering = ('street', 'country')
        verbose_name = _('Address')
        verbose_name_plural = _('Addresses')

    def __str__(self):
        return f'{self.city}, {self.street}, {self.country}'


__all__ = (
    'Country',
    'Address'
)
