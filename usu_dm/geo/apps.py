# -*- coding: UTF-8 -*-
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class GeoConfig(AppConfig):
    name = 'usu_dm.geo'
    label = 'dm_geo'
    verbose_name = _("Geo")
