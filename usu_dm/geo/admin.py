# -*- coding: UTF-8 -*-
from django.contrib import admin

from usu_dm.geo.models import Country, Address


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('name', 'alpha3_code')


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = ('street', 'city', 'state', 'county', 'country')
