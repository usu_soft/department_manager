# -*- coding: UTF-8 -*-
import factory

from usu_dm.geo.models import Address, Country


class CountryFactory(factory.DjangoModelFactory):
    class Meta:
        model = Country
        django_get_or_create = ('alpha3_code', )

    name = factory.Faker('country')
    alpha3_code = factory.Faker('country_code', representation='alpha-3')


class AddressFactory(factory.DjangoModelFactory):
    class Meta:
        model = Address

    street = factory.Faker('street_name')
    city = factory.Faker('city')
    state = factory.Faker('state')
    county = ''
    country = factory.SubFactory(CountryFactory)


__all__ = (
    'CountryFactory',
    'AddressFactory'
)
