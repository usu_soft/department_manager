# -*- coding: UTF-8 -*-
# Generated by Django 2.1.7 on 2019-02-17 18:23
from typing import TYPE_CHECKING

from django.db import migrations

from usu_dm.departments.factories import DepartmentFactory
from usu_dm.staff.models import Person
from usu_dm.staff.factories import PersonDepartmentFactory

if TYPE_CHECKING:
    from django.apps.registry import Apps
    from django.db.backends.base.schema import BaseDatabaseSchemaEditor


def initialize_data(apps: 'Apps', schema_editor: 'BaseDatabaseSchemaEditor') -> None:
    # initialize departments with leaders
    company = DepartmentFactory(name='Company', superior=None, leader__status=Person.ACTIVE_STATUS)

    development = DepartmentFactory(name='Development', superior=company, leader__status=Person.ACTIVE_STATUS)
    documentation = DepartmentFactory(name='Documentation', superior=development, leader__status=Person.ACTIVE_STATUS)
    testing = DepartmentFactory(name='Testing', superior=development, leader__status=Person.ACTIVE_STATUS)

    sales = DepartmentFactory(name='Sales', superior=company, leader__status=Person.ACTIVE_STATUS)
    czechia = DepartmentFactory(name='Czechia', superior=sales, leader__status=Person.ACTIVE_STATUS)
    germany = DepartmentFactory(name='Germany', superior=sales, leader__status=Person.ACTIVE_STATUS)

    marketing = DepartmentFactory(name='Marketing', superior=company, leader=sales.leader)
    advertisement = DepartmentFactory(name='Advertisement', superior=marketing, leader=germany.leader)

    # set leaders as members of its departments
    for dep in company, development, documentation, testing, sales, czechia, germany, marketing, advertisement:
        PersonDepartmentFactory(department=dep, person=dep.leader)

    # create staff
    for dep in company, development, documentation, testing, sales, czechia, germany, marketing, advertisement:
        # active staff
        for _ in range(3):
            PersonDepartmentFactory(department=dep, person__status=Person.ACTIVE_STATUS)

        # non-active staff
        for _ in range(2):
            PersonDepartmentFactory(department=dep, person__status=Person.NON_ACTIVE_STATUS)


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('dm_departments', '0002_auto_20190218_1836'),
        ('dm_geo', '0001_initial'),
        ('dm_staff', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(initialize_data, migrations.RunPython.noop, atomic=True),
    ]
