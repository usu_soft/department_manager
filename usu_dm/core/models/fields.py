# -*- coding: UTF-8 -*-
from uuid import uuid4
from django.db import models
from django.utils.translation import gettext_lazy as _


class UUIDField(models.UUIDField):
    def __init__(self, verbose_name=None, **kwargs):
        verbose_name = verbose_name if verbose_name else _('ID')
        kwargs['primary_key'] = True
        kwargs['default'] = uuid4
        kwargs['editable'] = False
        super().__init__(verbose_name, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        if kwargs['verbose_name'] == _('ID'):
            del kwargs['verbose_name']
        if kwargs['primary_key'] is True:
            del kwargs['primary_key']
        if kwargs['default'] == uuid4:
            del kwargs['default']
        if kwargs['editable'] is False:
            del kwargs['editable']
        return name, path, args, kwargs
