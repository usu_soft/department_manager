# -*- coding: UTF-8 -*-
import pytest
from anytree import find
from django.db.models import Sum

from usu_dm.core.homeworks import (
    get_condition, active_persons, max_salary, department_tree, department_tree_report, coordinates,
    leaders_with_salary_totals, leaders_with_salary_totals_report)
from usu_dm.staff.models import Person


class TestGetCondition:
    @pytest.mark.parametrize("criteria,expected_result", [
        ([{'firstname': 'John'}], "(firstname = 'John')"),
        ([{'firstname': 'John', 'lastname': 'Doe'}], "(firstname = 'John' AND lastname = 'Doe')"),
    ])
    def test_single_criteria(self, criteria, expected_result):
        assert get_condition(criteria) == expected_result

    def test_multiple_criteria(self):
        criteria = [{'firstname': 'John', 'lastname': 'Doe'}, {'firstname': 'Peter', 'lastname': 'Falk'}]
        expected = "(firstname = 'John' AND lastname = 'Doe') OR (firstname = 'Peter' AND lastname = 'Falk')"
        assert get_condition(criteria) == expected

    def test_invalid_attribute(self):
        criteria = [{'invalid': 1}]
        with pytest.raises(LookupError) as exc_info:
            get_condition(criteria)
        assert exc_info.value.args[0] == "The column 'invalid' does not exist."


@pytest.mark.django_db
def test_active_persons(person_factory, person_department_factory):
    # create persons
    walter = person_factory(firstname='Walter', lastname='White', status=Person.ACTIVE_STATUS)
    jessie = person_factory(firstname='Jessie', lastname='Pinkman', status=Person.ACTIVE_STATUS)
    saul = person_factory(firstname='Saul', lastname='Goodman', status=Person.ACTIVE_STATUS)
    hank = person_factory(firstname='Hank', lastname='Schrader', status=Person.NON_ACTIVE_STATUS)
    gustavo = person_factory(firstname='Gustavo', lastname='Fring', status=Person.ACTIVE_STATUS)
    # assign them to a department
    for person in walter, jessie, saul, hank:
        person_department_factory(department__name='Laboratory', department__superior=None, person=person)
    person_department_factory(department__name='Los Pollos', department__superior=None, person=gustavo)

    expected = [p.full_name for p in (saul, jessie, walter)]
    assert active_persons('Laboratory', Person.ACTIVE_STATUS) == expected


@pytest.mark.django_db
def test_max_salary(department_factory, person_department_factory):
    lab = department_factory(name='Laboratory', superior=None)
    los_pollos = department_factory(name='Los Pollos', superior=None)

    person_department_factory(department=lab, person__salary=50000)
    person_department_factory(department=lab, person__salary=20000)
    person_department_factory(department=lab, person__salary=10000)
    person_department_factory(department=los_pollos, person__salary=99000)

    assert max_salary('Laboratory') == 50000
    assert max_salary('Development') is None


@pytest.mark.django_db
class TestDepartmentTree:
    def test_single_root(self, department_factory):
        company = department_factory(name='Company', superior=None)
        development = department_factory(name='Development', superior=company)
        documentation = department_factory(name='Documentation', superior=development)
        testing = department_factory(name='Testing', superior=development)
        sales = department_factory(name='Sales', superior=company)
        czechia = department_factory(name='Czechia', superior=sales)

        tree = department_tree()

        assert len(tree) == 1

        root = tree[0]
        assert root.name == company.name

        # test the first level children
        assert sorted(n.name for n in root.children) == sorted(d.name for d in (development, sales))

        # test the second level children
        development_node = find(root, lambda n: n.name == development.name)
        assert sorted(n.name for n in development_node.children) == sorted(d.name for d in (documentation, testing))

        sales_node = find(root, lambda n: n.name == sales.name)
        assert len(sales_node.children) == 1
        assert sales_node.children[0].name == czechia.name

    def test_multiple_roots(self, department_factory):
        company = department_factory(name='Company', superior=None)
        development = department_factory(name='Development', superior=company)
        documentation = department_factory(name='Documentation', superior=development)
        testing = department_factory(name='Testing', superior=development)
        sales = department_factory(name='Sales', superior=company)
        czechia = department_factory(name='Czechia', superior=sales)

        los_pollos = department_factory(name='Los Pollos', superior=None)
        laboratory = department_factory(name='Laboratory', superior=los_pollos)

        tree = department_tree()

        assert len(tree) == 2

        # test `company` tree
        company_root = list(filter(lambda n: n.name == company.name, tree))[0]

        # test the first level children
        assert sorted(n.name for n in company_root.children) == sorted(d.name for d in (development, sales))

        # test the second level children
        development_node = find(company_root, lambda n: n.name == development.name)
        assert sorted(n.name for n in development_node.children) == sorted(d.name for d in (documentation, testing))

        sales_node = find(company_root, lambda n: n.name == sales.name)
        assert len(sales_node.children) == 1
        assert sales_node.children[0].name == czechia.name

        # test `los pollos` tree
        los_pollos_root = list(filter(lambda n: n.name == los_pollos.name, tree))[0]
        assert len(los_pollos_root.children) == 1
        assert los_pollos_root.children[0].name == laboratory.name


@pytest.mark.django_db
def test_department_tree_report(department_factory):
    company = department_factory(name='Company', superior=None)
    development = department_factory(name='Development', superior=company)
    documentation = department_factory(name='Documentation', superior=development)
    testing = department_factory(name='Testing', superior=development)
    sales = department_factory(name='Sales', superior=company)
    czechia = department_factory(name='Czechia', superior=sales)

    expected = 'Company\n├── Development\n│   ├── Documentation\n│   └── Testing\n└── Sales\n    └── Czechia\n'
    assert department_tree_report() == expected


def test_coordinates(mocker):
    location_mock = mocker.Mock()
    type(location_mock).latitude = mocker.PropertyMock(return_value=49.471341)
    type(location_mock).longtitude = mocker.PropertyMock(return_value=17.1167014)
    mocker.patch(
        'usu_dm.core.homeworks.Nominatim.geocode',
        return_value=location_mock
    )

    assert coordinates('user_agent', 'address') == (49.471341, 17.1167014)


def test_coordinates_for_non_existent_address(mocker):
    mocker.patch(
        'usu_dm.core.homeworks.Nominatim.geocode',
        return_value=None
    )

    assert coordinates('user_agent', 'address') is None


@pytest.mark.django_db
def test_leaders_with_salary_totals(department_factory, person_department_factory):
    company = department_factory(name='Company', superior=None)
    development = department_factory(name='Development', superior=company)
    documentation = department_factory(name='Documentation', superior=development)

    # add staff to departments
    for _ in range(3):
        person_department_factory(department=company)
        person_department_factory(department=development)
        person_department_factory(department=documentation)

    expected_company_data = (
        company.name,
        company.leader.full_name,
        company.persons.aggregate(salary_totals=Sum('salary'))['salary_totals'],
    )
    expected_development_data = (
        development.name,
        development.leader.full_name,
        development.persons.aggregate(salary_totals=Sum('salary'))['salary_totals'],
    )
    expected_documentation_data = (
        documentation.name,
        documentation.leader.full_name,
        documentation.persons.aggregate(salary_totals=Sum('salary'))['salary_totals'],
    )
    expected_data = (expected_company_data, expected_development_data, expected_documentation_data)

    assert sorted(leaders_with_salary_totals(), key=lambda x: x[0]) == sorted(expected_data, key=lambda x: x[0])


@pytest.mark.django_db
def test_leaders_with_salary_totals_report(department_factory, person_department_factory):
    company = department_factory(name='Company', superior=None)
    development = department_factory(name='Development', superior=company)
    documentation = department_factory(name='Documentation', superior=development)

    # add staff to departments
    for _ in range(3):
        person_department_factory(department=company)
        person_department_factory(department=development)
        person_department_factory(department=documentation)

    expected_company_data = (
        company.name,
        company.leader.full_name,
        company.persons.aggregate(salary_totals=Sum('salary'))['salary_totals'],
    )
    expected_development_data = (
        development.name,
        development.leader.full_name,
        development.persons.aggregate(salary_totals=Sum('salary'))['salary_totals'],
    )
    expected_documentation_data = (
        documentation.name,
        documentation.leader.full_name,
        documentation.persons.aggregate(salary_totals=Sum('salary'))['salary_totals'],
    )
    expected_data = '\n\n'.join(
        [f'Department: {totals[0]}\nLeader: {totals[1]}\nSalary: {totals[2]}'
         for totals
         in (expected_company_data, expected_development_data, expected_documentation_data)]
    )

    assert leaders_with_salary_totals_report() == expected_data
