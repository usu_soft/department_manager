# -*- coding: UTF-8 -*-
from decimal import Decimal
from typing import List, Dict, Any, Optional, Tuple

from anytree import Node, PreOrderIter, RenderTree
from django.core.exceptions import FieldDoesNotExist
from django.db import connection
from django.db.models import CharField, Value, Max, Case, When, F, Sum, OuterRef, Subquery
from django.db.models.functions import Concat
from geopy import Nominatim

from usu_dm.departments.models import Department
from usu_dm.staff.models import Person


def get_condition(criteria: List[Dict[str, Any]]) -> str:
    sql_tokens = []
    for c in criteria:
        criteria_sql_tokens = []
        for attr_name, value in c.items():
            try:
                cleaned_value = Person._meta.get_field(attr_name).get_db_prep_value(value, connection)
            except FieldDoesNotExist:
                raise LookupError(f"The column '{attr_name}' does not exist.")
            criteria_sql_tokens.append(f"{attr_name} = '{cleaned_value}'")
        if criteria_sql_tokens:
            sql_tokens.append('({})'.format(' AND '.join(criteria_sql_tokens)))
    return ' OR '.join(sql_tokens)


def active_persons(department_name: str, status: str) -> List[str]:
    return list(Person.objects.filter(
        departments__name=department_name, status=status
    ).order_by(
        'lastname', 'firstname'
    ).annotate(
        name=Concat('firstname', Value(' '), 'lastname', output_field=CharField())
    ).values_list('name', flat=True))


def max_salary(department_name: str) -> Optional[Decimal]:
    return Person.objects.filter(departments__name=department_name).aggregate(max_salary=Max('salary'))['max_salary']


def department_tree() -> List[Node]:
    # TODO There are places for optimizations, e.g. don't make DB queries in loop.
    department_roots = []
    departments = Department.objects.annotate(
        order=Case(
            When(superior=None, then=0),
            default=F('superior_id'))
    ).order_by('order')

    def find_parent_node(name: str) -> Optional[Node]:
        for r in department_roots:
            try:
                n = list(PreOrderIter(r, filter_=lambda n: n.name == name))[0]
            except IndexError:
                continue
            return n

    for department in departments:
        node = Node(department.name)

        superior_department = department.superior
        if superior_department is None:
            department_roots.append(node)
        else:
            parent = find_parent_node(superior_department.name)
            if parent:
                node.parent = parent
            else:
                pass  # FIXME add to queue that will be processed later

    return department_roots


def department_tree_report() -> str:
    """ Only one department root is supported. """
    for tree in department_tree():
        tree_repr = ''
        for pre, fill, node in RenderTree(tree):
            tree_repr += f'{pre}{node.name}\n'
        return tree_repr


def leaders_with_salary_totals() -> List[Tuple[str, str, Decimal]]:
    salary_totals = Department.objects.filter(
        pk=OuterRef('pk')
    ).annotate(salary_totals=Sum('persons__salary'))

    return list(Department.objects.annotate(
        department_name=F('name'),
        leader_name=Concat('leader__firstname', Value(' '), 'leader__lastname'),
        salary_totals=Subquery(salary_totals.values('salary_totals')[:1]),
    ).values_list('department_name', 'leader_name', 'salary_totals', named=True))


def leaders_with_salary_totals_report() -> str:
    return '\n\n'.join(
        [f'Department: {totals[0]}\nLeader: {totals[1]}\nSalary: {totals[2]}'
         for totals
         in leaders_with_salary_totals()]
    )


def coordinates(user_agent: str, address: str) -> Optional[Tuple[float, float]]:
    geo_locator = Nominatim(user_agent=user_agent)
    location = geo_locator.geocode(address)
    return location and (location.latitude, location.longtitude)


__all__ = (
    'get_condition',
    'active_persons',
    'max_salary',
    'department_tree',
    'department_tree_report',
    'leaders_with_salary_totals',
    'leaders_with_salary_totals_report',
    'coordinates'
)