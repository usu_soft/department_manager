# -*- coding: UTF-8 -*-
from django.urls import path

from usu_dm.core.views import IndexView

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
]