# USU Department Manager

## Installation

### Development

1. Clone this repository:
    1. `git clone git@gitlab.com:usu_soft/department_manager.git`
1. Install pipenv into global python `pip install pipenv`
1. *Optional:* Create new virtualenv and activate it.
1. Install python dependencies:
    1. Go to the project _repository root_ 
    1. Execute `pipenv sync --dev`. It will create new virtualenv unless you are not in some already.  
1. Create database
1. Create `.env` file in _repository root_ and set variables based on `.env.example` template.
1. run `python manage.py migrate`
1. run `python manage.py createsuperuser`
1. run `python manage.py runserver` and keep running

### Testing of homework tasks

1. Activate the virtualenv if any
1. run `python manage.py shell`
1. import
```
from usu_dm.core.homeworks import *
``` 