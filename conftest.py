# -*- coding: UTF-8 -*-
from pytest_factoryboy import register

from usu_dm.departments.factories import DepartmentFactory
from usu_dm.staff.factories import PersonFactory, PersonDepartmentFactory

register(DepartmentFactory)
register(PersonFactory)
register(PersonDepartmentFactory)
